const express = require('express');
const path = require('path');
const app = express();

app.get('*', function (request, response){
    response.sendFile(path.resolve(__dirname, 'public', 'index.html'))
})

//module.exports = app;

// const server = app.listen(3000, function() {
//     const host = server.address().address;
//     const port = server.address().port;
//     console.log('Example app listening at http://%s:%s', host, port);
// });  