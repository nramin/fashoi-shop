import axios from 'axios';

//Fetch items
export const FETCH_ITEMS = 'FETCH_ITEMS';
export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const FETCH_ITEMS_FAILURE = 'FETCH_ITEMS_FAILURE';
export const RESET_ACTIVE_ITEMS = 'RESET_ACTIVE_ITEMS';

const API_URL = 'http://localhost:3000';

export function fetchItems(page, limit, keywords) {
    if (page === undefined) {
        page = 0;
    } else if (page === 0) {
        page = 0;
    } else {
        page = page - 1;
    }

    if (limit === undefined) {
        limit = 10;
    }

    let skip = limit * page;

    if (keywords === undefined) {
        keywords = "";
    } else {
        keywords = `&keywords=${keywords}`;
    }

    const request = axios.get(`${API_URL}/items/?skip=${skip}&limit=${limit}${keywords}`);

    return {
        type: FETCH_ITEMS,
        payload: request
    };
}

export function fetchItemsSuccess(items) {
    return {
        type: FETCH_ITEMS_SUCCESS,
        payload: items
    };
}

export function fetchItemsFailure(error) {
    return {
        type: FETCH_ITEMS_FAILURE,
        payload: error
    };
}

export function resetActiveItems() {
    return {
        type: RESET_ACTIVE_ITEMS
    }
}
