import axios from 'axios';

//Fetch item
export const FETCH_ITEM = 'FETCH_ITEM';
export const FETCH_ITEM_SUCCESS = 'FETCH_ITEM_SUCCESS';
export const FETCH_ITEM_FAILURE = 'FETCH_ITEM_FAILURE';
export const RESET_ACTIVE_ITEM = 'RESET_ACTIVE_ITEM';

const API_URL = 'http://localhost:3000';

export function fetchItem(id) {
    const request = axios.get(`${API_URL}/items/${id}`);

    return {
        type: FETCH_ITEM,
        payload: request
    };
}

export function fetchItemSuccess(item) {
    return {
        type: FETCH_ITEM_SUCCESS,
        payload: item
    };
}

export function fetchItemFailure(error) {
    return {
        type: FETCH_ITEM_FAILURE,
        payload: error
    };
}

export function resetActiveItem() {
    return {
        type: RESET_ACTIVE_ITEM
    }
}
