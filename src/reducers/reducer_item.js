import {
    FETCH_ITEM, FETCH_ITEM_SUCCESS, FETCH_ITEM_FAILURE, RESET_ACTIVE_ITEM
} from '../actions/item';

const INITIAL_STATE = { item: {featuredPhoto: {mediumURL: null, largeURL: null}}, error: null, loading: false };

export default function(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_ITEM:
            return { ...state, item: {...state.item}, error: null, loading: true };
        case FETCH_ITEM_SUCCESS:
            return { ...state, item: action.payload, error: null, loading: false };
        case FETCH_ITEM_FAILURE:
            error = action.payload || {message: action.payload.message};
            return { ...state, item: {}, error: error, loading: false };
        case RESET_ACTIVE_ITEM:
            return { ...state, item: {}, error: null, loading: false };
        default:
            return state;
    }
}