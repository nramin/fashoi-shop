import {
    FETCH_ITEMS, FETCH_ITEMS_SUCCESS, FETCH_ITEMS_FAILURE, RESET_ACTIVE_ITEMS
} from '../actions/items';

const INITIAL_STATE = { items: [], totalCount: 0, error: null, loading: false };

export default function(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_ITEMS:
            return { ...state, items: [], totalCount: 0, error: null, loading: true };
        case FETCH_ITEMS_SUCCESS:
            return { ...state, items: action.payload.items, totalCount: action.payload.totalCount, error: null, loading: false };
        case FETCH_ITEMS_FAILURE:
            return { ...state, items: [], totalCount: 0, error: error, loading: false };
        case RESET_ACTIVE_ITEMS:
            return { ...state, items: [], totalCount: 0, error: null, loading: false };
        default:
            return state;
    }
}