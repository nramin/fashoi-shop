import { combineReducers } from 'redux';
import ItemReducer from './reducer_item';
import ItemsReducer from './reducer_items';

const rootReducer = combineReducers({
    item: ItemReducer,
    items: ItemsReducer
});

export default rootReducer;