import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux';
import reducer from './reducers';
import configureStore from './store/configureStore.js';
import { withRouter } from 'react-router-dom';
import './scss/main.scss'

import App from './pages/App';
import Home from './pages/Home';
import About from './pages/About';
import Item from './pages/Item';
import Browse from './pages/Browse';

const store = configureStore();
const NonBlockApp = withRouter(App);

render(
    <Provider store={store}>
        <Router>
            <NonBlockApp>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/item/:id" component={Item} />
                    <Route path="/browse/:keywords/:page?" component={Browse} />
                </Switch>
            </NonBlockApp>
        </Router>
    </Provider>, 
    document.getElementById('body')
);