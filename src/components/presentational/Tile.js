import React from 'react';
import { Link } from 'react-router-dom';

const Tile = (props) => {
    return (
        <div className="col-md-4 col-lg-3 shop-tile">
            <Link className="thumbnail" to={"/item/" + props.titleId}>
                <img src={props.featuredPhoto.mediumUrl} />
            </Link>

            <Link to={"/item/" + props.titleId}>{props.title}</Link>
        </div>
    )
};

export default Tile;