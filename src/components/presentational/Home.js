import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Tile from './Tile';

class Home extends Component {
  static contextTypes = {
    activeItems: PropTypes.object
  };

  componentWillUnmount() {
    //Important! If your component is navigating based on some global state(from say componentWillReceiveProps)
    //always reset that global state back to null when you REMOUNT
     this.props.resetMe();
  }

  componentDidMount() {
    this.props.fetchItems(0, 10);
  }

  renderItems(items) {
    return items.map((item) => {
      return (
        <li key={item.asin}><Link to={"/item/" + item.asin}>{item.asin}</Link></li>
      );
    });
  }

  render() {
    const { items, loading, error } = this.props.activeItems;

    // if(loading) {
    //     console.log("loading yo");
    //     return <div className="container"><h1>Posts</h1><h3>Loading...</h3></div>      
    //   } else if(error) {
    //     return <div className="alert alert-danger">Error: {error.message}</div>
    // }

    return (
      <div className="container">
        {this.renderItems(items)}
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
      </div>
    );
  }
}

export default Home;