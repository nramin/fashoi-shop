import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class Item extends Component {
  static contextTypes = {
    activeItem: PropTypes.shape({
      featuredPhoto: PropTypes.arrayOf(PropTypes.string)
    }),
    id: PropTypes.string
  };

  componentWillUnmount() {
    //Important! If your component is navigating based on some global state(from say componentWillReceiveProps)
    //always reset that global state back to null when you REMOUNT
     this.props.resetMe();
  }

  componentDidMount() {
    this.props.fetchItem(this.props.id);
  }

  renderPhotos(photos) {
    return photos.map((photo) => {
      return (
        <li key={photo}>{photo}</li>
      );
    });
  }

  render() {
    const item = this.props.activeItem.item;
    console.log(item);

    return (
      <div className="shop-view-item">
        <div className="row">
          <div className="col-lg-6">

          </div>
          <div className="col-lg-6">

          </div>
          {item.title}
          {item.featuredPhoto.mediumURL}
        </div>
      </div>
    );
  }
}

export default Item;