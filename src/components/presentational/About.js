import React from 'react';
import { Link } from 'react-router-dom';

const About = () => {
    return (
        <div>
            <h4>About Page</h4>
            <Link to="/">Home</Link>
            <Link to="/about">About</Link>
        </div>
    );
};

export default About;