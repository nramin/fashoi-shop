import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Tile from './Tile';

class Browse extends Component {
  static contextTypes = {
    activeItems: PropTypes.object,
    totalCount: PropTypes.string,
    keywords: PropTypes.string,
    page: PropTypes.string
  };

  componentWillUnmount() {
    //Important! If your component is navigating based on some global state(from say componentWillReceiveProps)
    //always reset that global state back to null when you REMOUNT
     this.props.resetMe();
  }

  componentDidMount() {
    this.props.fetchItems(this.props.page, 10, this.props.keywords);
  }

  renderItems(items) {
    return items.map((item) => {
      return (
        <Tile key={item.asin} {...item} />
      );
    });
  }

  renderPrevPageLink() {
    const prevPageNumber = (parseInt(this.props.page) - parseInt(1));
    let prevPageLink = null;
    if (parseInt(this.props.page) > 1) {
      prevPageLink = <button onClick={() => this.props.history.push("/browse/" + this.props.keywords + "/" + prevPageNumber)}>Prev</button>
    } else {
      prevPageLink = <button disabled>Prev</button>
    }
    return prevPageLink;
  }

  renderNextPageLink(totalCount) {
    const nextPageNumber = (parseInt(this.props.page) + parseInt(1));
    let nextPageLink = null;
    if (parseInt(this.props.page) < Math.ceil(totalCount / 10)) {
      nextPageLink = <button onClick={() => this.props.history.push("/browse/" + this.props.keywords + "/" + nextPageNumber)}>Next</button>
    } else {
      nextPageLink = <button disabled>Next</button>
    }
    return nextPageLink;
  }

  render() {
    const items = this.props.activeItems.items;
    const totalCount = this.props.activeItems.totalCount;

    return (
      <div className="browse-catalog">
        <div className="row">
          {this.renderItems(items)}
        </div>
        {this.renderPrevPageLink()}
        {this.renderNextPageLink(totalCount)}
      </div>
    );
  }
}

export default Browse;