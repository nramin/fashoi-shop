import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Browse from '../presentational/Browse.js';
import { fetchItems, fetchItemsSuccess, fetchItemsFailure, resetActiveItems } from '../../actions/items';

function mapStateToProps(globalState, ownProps) {
  return {
    activeItems: globalState.items,
    totalCount: globalState.totalCount,
    keywords: ownProps.match.params.keywords,
    page: (ownProps.match.params.page === undefined) ? 1 : ownProps.match.params.page
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchItems: (page, limit, keywords) => {
      dispatch(fetchItems(page, limit, keywords))
        .then((result) => {
          // Note: Error's "data" is in result.payload.response.data (inside "response")
          // success's "data" is in result.payload.data
          if (result.payload.response && result.payload.response.status !== 200) {
            dispatch(fetchItemsFailure(result.payload.response.data));
          } else {
            dispatch(fetchItemsSuccess(result.payload.data))
          }
        })
    },
    resetMe: () => {
      //clean up both activePost(currrently open) and deletedPost(open and being deleted) states
      dispatch(resetActiveItems());
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Browse);