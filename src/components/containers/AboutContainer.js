import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import About from '../presentational/About.js';


const mapStateToProps = () => {
  return {};
}

export default connect(mapStateToProps)(About);