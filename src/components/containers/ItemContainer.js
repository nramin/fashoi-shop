import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Item from '../presentational/Item.js';
import { fetchItem, fetchItemSuccess, fetchItemFailure, resetActiveItem } from '../../actions/item';

function mapStateToProps(globalState, ownProps) {
  return {
    activeItem: globalState.item,
    id: ownProps.match.params.id
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchItem: (id) => {
      dispatch(fetchItem(id))
        .then((result) => {
          // Note: Error's "data" is in result.payload.response.data (inside "response")
          // success's "data" is in result.payload.data
          if (result.payload.response && result.payload.response.status !== 200) {
            dispatch(fetchItemFailure(result.payload.response.data));
          } else {
            dispatch(fetchItemSuccess(result.payload.data))
          }
        })
    },
    resetMe: () => {
      //clean up both activePost(currrently open) and deletedPost(open and being deleted) states
      dispatch(resetActiveItem());
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Item);

// const mapStateToProps = () => {
//   return {};
// }

//export default connect(mapStateToProps)(Item);