import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import App from '../presentational/App.js';
import { withRouter } from "react-router-dom";

const mapStateToProps = () => {
  return {};
}

export default withRouter(connect(mapStateToProps, null)(App));