import React from 'react';
import { Component } from 'react';
import HomeContainer from '../components/containers/HomeContainer';

const Home = (props) => {
    return (
        <div>
            <HomeContainer {...props} />
        </div>
    );
};

export default Home;