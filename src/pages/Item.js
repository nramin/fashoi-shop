import React from 'react';
import { Component } from 'react';
import ItemContainer from '../components/containers/ItemContainer';

const Item = (props) => {
    return (
        <div>
            <ItemContainer {...props} />
        </div>
    );
};

export default Item;