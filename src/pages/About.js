import React from 'react';
import { Component } from 'react';
import AboutContainer from '../components/containers/AboutContainer';

const About = (props) => {
    return (
        <div>
            <AboutContainer />
        </div>
    );
};

export default About;