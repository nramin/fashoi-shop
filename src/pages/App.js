import React from 'react';
import { Component } from 'react';
import AppContainer from '../components/containers/AppContainer';
import { withRouter } from 'react-router-dom';

const App = (props) => {
    const NonBlockAppContainer = withRouter(AppContainer);
    return (
        <NonBlockAppContainer>
            {props.children}
        </NonBlockAppContainer>
    );
};

export default App;