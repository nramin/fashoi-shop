import React from 'react';
import { Component } from 'react';
import BrowseContainer from '../components/containers/BrowseContainer';

const Browse = (props) => {
    return (
        <div>
            <BrowseContainer {...props} />
        </div>
    );
};

export default Browse;